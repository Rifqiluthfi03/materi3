module restapi

go 1.18

require (
	//indirect
	github.com/gorilla/mux v1.8.0 
	github.com/julienschmidt/httprouter v1.3.0 
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 
)
